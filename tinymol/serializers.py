'''
Created on Mar 12, 2014

@author: lifej
'''

from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from rest_framework import serializers
from .models import Tag, Molecule, Name, Code, RealValue, IntValue, Keyword
from .tools import to_smiles

class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'label')

class NameSerializer(serializers.ModelSerializer):
    type = serializers.StringRelatedField()
    language = serializers.StringRelatedField()
    class Meta:
        model = Name
        fields = ('id', 'type', 'value', 'language')

class CodeSerializer(serializers.ModelSerializer):
    type = serializers.StringRelatedField()
    class Meta:
        model = Code
        fields = ('id', 'type', 'value')

class RealValueSerializer(serializers.ModelSerializer):
    type = serializers.StringRelatedField()
    class Meta:
        model = RealValue
        fields = ('id', 'type', 'value')

class IntValueSerializer(serializers.ModelSerializer):
    type = serializers.StringRelatedField()
    class Meta:
        model = IntValue
        fields = ('id', 'type', 'value')

class SmilesSerializer(serializers.ModelSerializer):    
    smiles = serializers.CharField(source='get_smiles')
    formula = serializers.CharField(source='get_formula')    
    weight = serializers.FloatField(source='get_weight')    
    names = NameSerializer(many=True)
    codes = CodeSerializer(many=True)
    real_props = RealValueSerializer(many=True)
    int_props = IntValueSerializer(many=True)

    class Meta:
        model = Molecule
        fields = ('id', 'smiles', 'inchi_key', 'formula', 'weight', 'names', 'codes', 'real_props', 'int_props')

class MoleculeSerializer(serializers.ModelSerializer):    
    structure = serializers.CharField(source='get_mdl')
    formula = serializers.CharField(source='get_formula')
    weight = serializers.FloatField(source='get_weight')
    names = NameSerializer(many=True)
    codes = CodeSerializer(many=True)
    real_props = RealValueSerializer(many=True)
    int_props = IntValueSerializer(many=True)

    class Meta:
        model = Molecule
        fields = ('id', 'structure', 'inchi_key', 'formula', 'weight', 'names', 'codes', 'real_props', 'int_props')

class KeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keyword
        fields = ('id', 'word')
