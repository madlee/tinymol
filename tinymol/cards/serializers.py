'''
Created on Mar 12, 2014

@author: lifej
'''

from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from django.db.models import Sum, Count

from rest_framework import serializers

from madlee.serializers import SimpleUserSerializer

from ..models import Name
from .models import Board, Molecule, Rate, Comment
from ..serializers import NameSerializer, CodeSerializer, IntValueSerializer, RealValueSerializer

class CommentSerializer(serializers.ModelSerializer):
    author = SimpleUserSerializer()
    parent = serializers.StringRelatedField()

    class Meta:
        model = Comment
        fields = ('id', 'author', 'comment', 'timestamp0', 'parent', 'is_active')

class RateSerializer(serializers.ModelSerializer):
    author = SimpleUserSerializer()

    class Meta:
        model = Rate
        fields = ('id', 'author', 'rate', 'timestamp0')
   
class BoardSerializer(serializers.ModelSerializer):
    owner = SimpleUserSerializer()
    author = SimpleUserSerializer()

    class Meta:
        model = Board
        fields = ('id', 'owner', 'name', 'note', 'author', 'timestamp0', 'timestamp1')


class MoleculeSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    structure = serializers.SerializerMethodField()
    formula = serializers.SerializerMethodField()
    weight = serializers.SerializerMethodField()
    properties = serializers.SerializerMethodField()
    rate = serializers.SerializerMethodField()
    
    comments = CommentSerializer(many=True)

    def get_structure(self, obj):
        return obj.mol.get_mdl()

    def get_formula(self, obj):
        return obj.mol.get_formula()

    def get_weight(self, obj):
        return obj.mol.get_weight()

    def get_name(self, obj):
        try:
            name = obj.mol.names.get(type=obj.board.mol_name)
            return name.value
        except Name.DoesNotExist:
            name = obj.mol.codes.get(type=obj.board.mol_name)
            return name.value

    def get_properties(self, obj):
        result = []
        all_tags =  obj.board.mol_props.all()
        names = obj.mol.names.filter(type__in = all_tags)
        result += NameSerializer(names, many=True).data
        codes = obj.mol.codes.filter(type__in = all_tags)
        result += CodeSerializer(codes, many=True).data
        int_props = obj.mol.int_props.filter(type__in = all_tags)
        result += IntValueSerializer(int_props, many=True).data
        real_props = obj.mol.real_props.filter(type__in = all_tags)
        result += RealValueSerializer(real_props, many=True).data
        return result

    def get_rate(self, obj):
        return {'sum': obj.rate_sum, 'n': obj.rate_count, 'avg': obj.rate_avg}

    class Meta:
        model = Molecule
        fields = ('id', 'name', 'structure', 'formula', 'weight', 'board', 'properties', 'rate', 'comments')


