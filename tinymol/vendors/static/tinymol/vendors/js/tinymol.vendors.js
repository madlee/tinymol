requirejs.config({  
    paths : {  
        jquery      : 'http://cdn.staticfile.org/jquery/1.11.1/jquery.min',  
        bootstrap   : 'http://cdn.staticfile.org/twitter-bootstrap/3.2.0/js/bootstrap.min',  
        Madlee      : '/static/madlee/js/madlee',
        domReady    : '/static/requireJS/domReady'
    }  
});  


require(['Madlee'], function(Madlee) {

  var tinymolControllers = angular.module('tinymolControllers', []);

  var $ = jQuery;

  tinymolControllers.controller('SearchCtrl', ['$scope', '$http', '$location', 
    function ($scope, $http, $location) {
    	var par = $location.search()
      if (typeof(par.page) === 'undefined') {
        par.page = 1
      }
      if (typeof(par.text) === 'undefined') {
        par.text = ''
      }

      $scope.count = 0
      $scope.page = 1

      $scope.set_page = function (pageNo) {
        par.page = pageNo
        $http.get('/tinymol/mol', {params: par}).success(function(data) {
          var mols = data.results
          for (var i = 0; i < mols.length; ++i) {
            mols[i].structure = TME.parseMol(mols[i].structure)
          }
          $scope.text = par.text
          $scope.mols = mols
          $scope.count = data.count
          $scope.page = par.page
        });
      }

      $scope.page_changed = function() {
        $scope.set_page($scope.page)
      }

      $scope.set_page(par.page)
    }
  ]);

  tinymolControllers.controller('UploadCtrl', ['$scope', '$http', '$location',
    function ($scope, $http, $location) {
      
    }
  ]);

  tinymolControllers.controller('ProductCtrl', ['$scope', '$http', '$location',
    function ($scope, $http, $location) {
      
    }
  ]);

  tinymolControllers.controller('AuthCtrl', ['$scope', '$http',
    function ($scope, $http) {
      $scope.user = null

      $http.get('/madlee/user/who_am_i').success(function(data) {
        $scope.user = data
      })

      $scope.login = function($event) {
        var dlg = $($event.target).closest(".modal")
        if (validate_all(dlg)) {
          var pars = collect_value(dlg)
          $http({url:"/madlee/user/login", method:'POST', data:pars}).success(function(data) {
            $scope.user = data
            dlg.modal('hide')
          })
        }
      }

      $scope.logout = function($event) {
        $http.post("/madlee/user/logout")
        $scope.user = null
        window.location = "#"
      }
    }
  ]);



  var filters = angular.module('tinymolFilters', [])

  filters.filter('formula', function() {
    return function(input) {
      var digits = /[A-Za-z]\d+/g;

      var result = []
      var last_pos = 0
      var match = null;
      while ((match = digits.exec(input)) != null) {
        result.push(input.substr(last_pos, match.index+1-last_pos))
        result.push('<sub>')
        result.push(match[0].substr(1))
        result.push('</sub>')
        last_pos = match.index + match[0].length
    }
    result.push(input.substr(last_pos))
      return result.join('')
    };
  });

  filters.filter('highlight', function() {
    return function(input, pattern) {
      var result = []
      var n = pattern.length
      var i0 = 0
      for (;;) {
        var i1 = input.indexOf(pattern, i0)
        if (i1 > 0) {
          result.push(input.substring(i0, i1))
          result.push('<mark>')
          result.push(input.substring(i1, i1+n))
          result.push('</mark>')
          i0 = i1+n
        }
        else {
          result.push(input.substring(i0))
          break
        }
      }
      return result.join('')
    }

  });


  var directives = angular.module('tinymolDirectives', [])

  directives.directive('tinymol', function() {
    return function(scope, elements, iAttr) {
      var mol = scope.mol
      var canvas = elements[0]
      TME.drawMol(canvas, mol.structure)
    };
  });



  var query_request = null;
  var condition = null;

  $(document).ready(function() {

    $(".fa-spinner").hide()
    
    $("#btn-txt-search").click(function(evt) {
      var txt = Madlee.required_value("#txt-search");
      if (txt != null) {
        window.location="#/search?text=" + txt
      }
      return false;
    });

    $('#btn-ss-submit').click(function(evt) {
      var mol = window.editor.mol.toString()
      $.ajax()

    });


    
    var div = $("#editor");
    div.find(".btn-default").css("border-color", "#FFFFFF");
    window.editor = new TME.Editor(div);
    window.editor.load(URL_SAMPLE_MOL);

  });

  var app = angular.module('TinymolApp', [
    'ngRoute',
    'ngSanitize',
    'ui.bootstrap',
    'tinymolControllers',
    'tinymolFilters',
    'tinymolDirectives'
  ]);

  app.config(['$routeProvider',
    function($routeProvider) {
      $routeProvider.
        when('/search/', {
          templateUrl: 'search.html',
          controller: 'SearchCtrl'
        }).
        when('/upload/', {
          templateUrl: 'upload.html',
          controller: 'UploadCtrl'
        }).
        when('/product/', {
          templateUrl: 'product.html',
          controller: 'ProductCtrl'
        }).
        otherwise({
          redirectTo: '/search/'
        });
    }
  ]);

  app.config(['$httpProvider', function ($httpProvider) {
      $httpProvider.defaults.xsrfCookieName = 'csrftoken';
      $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

      $httpProvider.defaults.transformRequest = function(data){
          if (data === undefined) {
              return data;
          }
          return $.param(data);
      }
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
  }]);


  app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
  }])

  require(['domReady!'],function(document){
      angular.bootstrap(document,['TinymolApp']);
  });

})
