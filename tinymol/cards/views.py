from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from django.shortcuts import render
from django.db import transaction
from django.db.models import Sum

from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route

from madlee.views import rest_view

from .serializers import *


class BoardViewSet(viewsets.ModelViewSet):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer

    @list_route(methods=['post'])
    @rest_view
    @transaction.atomic
    def new_board(self, request):
        name = request.POST['name']
        note = request.POST['note']
        mol_name_id = request.POST['mol_name[id]']
        board = Board()
        board.name = name
        board.note = note
        board.author = board.owner = request.user
        board.mol_name_id = mol_name_id
        board.save()
        board.members.add(request.user)
        return {'result': BoardSerializer(board).data}

    def get_queryset(self):
        queryset = self.queryset
        return queryset.filter(members=self.request.user)


class MoleculeViewSet(viewsets.ModelViewSet):
    queryset = Molecule.objects.all()
    serializer_class = MoleculeSerializer

    def get_queryset(self):
        queryset = self.queryset
        return queryset

    @detail_route(methods=['post'])
    @rest_view
    def set_rate(self, request, boardid, pk=None):
        record = self.get_object()

        # print (boardid, record.board.id)
        assert record.board.id == int(boardid)

        user = request.user
        rate_value = int(request.POST['rate'])

        try:
            rate = Rate.objects.get(mol=record, author=user)
        except Rate.DoesNotExist:
            rate = Rate(mol=record, author=user)
        rate.rate = rate_value
        rate.save()

        comment = Comment()
        comment.mol = record
        comment.author = user
        comment.comment = "Set %(rate_value)d stars." % {'rate_value':rate_value}
        comment.save()

        record.rate_count = record.rates.all().count()
        record.rate_sum = record.rates.all().aggregate(Sum('rate'))['rate__sum']
        record.rate_avg = record.rate_sum / record.rate_count
        record.save()

        rate = RateSerializer(rate).data
        comment = CommentSerializer(comment).data
        return {'rate': rate, 'comment': comment, 
            'stat': {'sum': record.rate_sum, 'n': record.rate_count, 'avg': record.rate_avg}}


    @detail_route(methods=['post'])
    @rest_view
    def add_comment(self, request, boardid, pk=None):
        comment = Comment()
        comment.mol_id = pk
        comment.comment = request.POST['comment']
        comment.author = request.user
        comment.save()
        comment = CommentSerializer(comment).data
        return {'comment': comment}
    
   
class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def get_queryset(self):
        queryset = self.queryset
        boardid = self.kwargs['boardid']
        molid = self.kwargs['molid']
        return queryset.filter(board__id=boardid, mol__id=mol, is_active=True)

    @detail_route(methods=['post'])    
    def disable(self, request, pk=None):
        pass

