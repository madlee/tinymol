from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function


'''
Created on Apr 12, 2013

@author: lifej
'''

from django.conf.urls import patterns, url, include
from rest_framework import routers
from tinymol.cards.views import BoardViewSet, MoleculeViewSet, CommentViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'board', BoardViewSet)

router_mol = routers.DefaultRouter(trailing_slash=False)
router_mol.register(r'mol', MoleculeViewSet)

router_comment = routers.DefaultRouter(trailing_slash=False)
router_mol.register(r'comment', CommentViewSet)


urlpatterns = patterns('tinymol.cards.views',
	url(r'^board/(?P<boardid>\d+)/mol/(?P<molid>\d+)/', include(router_comment.urls)),
	url(r'^board/(?P<boardid>\d+)/', include(router_mol.urls)),
    url(r'', include(router.urls)),
)

