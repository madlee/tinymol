from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from django.contrib import admin
from .models import Board, Rate, Comment, Molecule

class BoardAdmin(admin.ModelAdmin):
    pass
admin.site.register(Board, BoardAdmin)

class RateAdmin(admin.ModelAdmin):
    pass
admin.site.register(Rate, RateAdmin)

class CommentAdmin(admin.ModelAdmin):
    pass
admin.site.register(Comment, CommentAdmin)


class MoleculeAdmin(admin.ModelAdmin):
    pass
admin.site.register(Molecule, MoleculeAdmin)