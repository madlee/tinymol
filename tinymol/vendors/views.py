from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from django.shortcuts import render
from django.db import transaction
from django.db.models import Sum
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt

from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route

from madlee.views import rest_view

from .serializers import *
from ..models import Molecule
from ..tools import parse_mol

# Create your views here.

class VendorViewSet(viewsets.ModelViewSet):
    queryset = Vendor.objects.all()
    serializer_class = VendorSerializer

@csrf_exempt
@rest_view
def structure_search(request):
	method = request.REQUEST['method']
	molfile = request.REQUEST['molfile']
	mol = parse_mol(molfile)
