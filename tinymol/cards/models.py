from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from django.db import models
from django.conf import settings
from django.utils.encoding import python_2_unicode_compatible
from ..models import Molecule as BasicMolecule, Tag, Keyword

User = settings.AUTH_USER_MODEL

@python_2_unicode_compatible
class Board(models.Model):
    owner = models.ForeignKey(User, related_name='+')
    name = models.TextField()
    
    note = models.TextField()

    mol_name = models.ForeignKey(Tag, related_name='+')
    mol_props = models.ManyToManyField(Tag, related_name='+')
    members = models.ManyToManyField(User, related_name='+')

    author = models.ForeignKey(User, related_name='+')
    timestamp0 = models.DateTimeField(auto_now_add=True)
    timestamp1 = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Molecule(models.Model):
    mol =  models.ForeignKey(BasicMolecule)
    board = models.ForeignKey(Board, related_name='mols')

    rate_sum    = models.IntegerField(default=0)
    rate_count  = models.IntegerField(default=0)
    rate_avg    = models.DecimalField(decimal_places=1, max_digits=2, default=None, null=True, blank=True)

    def __str__(self):
        return str(self.mol)

    class Meta:
        unique_together = ('mol', 'board'),

@python_2_unicode_compatible
class Rate(models.Model):
    mol = models.ForeignKey(Molecule, related_name='rates')
    rate = models.IntegerField()

    author = models.ForeignKey(User, db_index=True)
    timestamp0 = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '*' * self.rate

    class Meta:
        unique_together = ('mol', 'author')


@python_2_unicode_compatible
class Comment(models.Model):
    mol = models.ForeignKey(Molecule, null=True, related_name="comments")
    comment = models.TextField()

    author = models.ForeignKey(User)
    timestamp0 = models.DateTimeField(auto_now=True)
    parent = models.ForeignKey('self', null=True, blank=True, default=None, related_name='+')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.comment

