from django.conf.urls import patterns, include, url
from django.contrib import admin
from madlee.views import render_html

urlpatterns = patterns('',
	url(r'^(.+)\.html$', render_html),
	url(r'^(.+)\.form$', render_html, {'basic_path': '%s.form'}),

	url(r'^$', 'tinymol.views.index'),

    url(r'^tinymol/', include('tinymol.urls', namespace='tinymol' )),
    url(r'^madlee/', include('madlee.urls', namespace='madlee')),
    url(r'^admin/', include(admin.site.urls)),
)


