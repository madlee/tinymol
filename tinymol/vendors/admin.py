from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from django.contrib import admin
from .models import Vendor, Brand, Product

class VendorAdmin(admin.ModelAdmin):
    pass
admin.site.register(Vendor, VendorAdmin)


class BrandAdmin(admin.ModelAdmin):
    pass
admin.site.register(Brand, BrandAdmin)



class ProductAdmin(admin.ModelAdmin):
    pass
admin.site.register(Product, ProductAdmin)
