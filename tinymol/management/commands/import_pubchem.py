from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

import gzip
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from rdkit import Chem

from tinymol.tools import to_smiles
from tinymol.models import *


NAMES = 'PUBCHEM_IUPAC_NAME'
CODES = 'PUBCHEM_MOLECULAR_FORMULA SMILES INCHIKEY'
REAL_VALUES = 'PUBCHEM_XLOGP3 PUBCHEM_EXACT_MASS PUBCHEM_CACTVS_TPSA PUBCHEM_XLOGP3_AA MW'
INT_VALUES = 'PUBCHEM_CACTVS_HBOND_ACCEPTOR PUBCHEM_CACTVS_HBOND_DONOR PUBCHEM_CACTVS_ROTATABLE_BOND PUBCHEM_HEAVY_ATOM_COUNT'

ALL_TAGS = [NAMES, CODES, REAL_VALUES, INT_VALUES]

def prepare_tags(all_tags):
    def parse_tag(k):
        if k.startswith('PUBCHEM_'):
            k = k[8:]
        k = k.replace('_', ' ')
        return Tag.objects.get_or_create(label=k)

    def parse_tags(labels):
        result = {}
        for k in labels.split():
            v, _ = parse_tag(k)
            result[k] = v
        return result

    result = []
    for lables in all_tags:
        result.append(parse_tags(lables))
    return result

class Command(BaseCommand):

    def handle(self, *args, **options):
        names, codes, real_values, int_values = prepare_tags(ALL_TAGS)
        tag_language, _ = Tag.objects.get_or_create(label='en-us')
        code_smiles = codes['SMILES']
        code_inchikey = codes['INCHIKEY']
        int_values['PUBCHEM_COMPOUND_CID'], _ = Tag.objects.get_or_create(label='PUBCHEM CID')

        for filename in args:
            with transaction.atomic():
                if filename.endswith('.gz'):
                    file = gzip.open(filename, 'r')
                    data_source = Chem.ForwardSDMolSupplier(file)
                else: 
                    data_source = Chem.SDMolSupplier(filename)

                for mol in data_source:
                    mol = Chem.RemoveHs(mol)
                    properties = dict([(k, mol.GetProp(k)) for k in mol.GetPropNames()])
                    for k in properties.keys():
                        mol.ClearProp(k)

                    record = Molecule()
                    record.structure = mol
                    record.save()

                    for k, prop in properties.iteritems():
                        if k in names:
                            name, _ = Name.objects.get_or_create(type=names[k], value=prop, language=tag_language)
                            record.names.add(name)

                        if k in codes:
                            code, _ = Code.objects.get_or_create(type=codes[k], value=prop)
                            record.codes.add(code)

                        if k in real_values:
                            prop = float(prop)
                            RealValue(type=real_values[k], mol=record, value=prop).save()

                        if k in int_values:
                            prop = int(prop)
                            IntValue(type=int_values[k], mol=record, value=prop).save()

                    code, _ = Code.objects.get_or_create(type=code_smiles, value=to_smiles(mol))
                    record.codes.add(code)
                    code, _ = Code.objects.get_or_create(type=code_inchikey, value=record.inchi_key)
                    record.codes.add(code)