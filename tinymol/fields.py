from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from base64 import b64decode, b64encode

from django.db import models
from django.utils.six import with_metaclass
from django.utils.encoding import force_bytes
from django.utils import six
from django.utils.translation import ugettext_lazy as _

from .tools import Molecule, ExplicitBitVect

class MoleculeField(with_metaclass(models.SubfieldBase, models.Field)):
    description = _("Molecule field")
    
    __metaclass__ = models.SubfieldBase

    def __init__(self, *args, **kwargs):
        kwargs['editable'] = False
        super(MoleculeField, self).__init__(*args, **kwargs)
        
    def get_internal_type(self):
        return "BinaryField"

    def get_db_prep_value(self, value, connection, prepared=False):
        if value is not None:
            value = value.ToBinary()
            return connection.Database.Binary(value)
        else:
            return value
    
    def to_python(self, value):
        if isinstance(value, Molecule):
            return value
        else:
            if value:
                if isinstance(value, six.text_type):
                    # If it's a string, it should be base64-encoded raw data
                    value = b64decode(force_bytes(value))
                else:
                    value = bytes(value)

                return Molecule(value)
            else:
                return Molecule()
        

class BitsetField(models.Field):
    description = _("Bitset field")
    
    __metaclass__ = models.SubfieldBase
    
    def __init__(self, *args, **kwargs):
        kwargs['editable'] = False
        super(BitsetField, self).__init__(*args, **kwargs)
        
    def get_internal_type(self):
        return "BinaryField"

    def get_db_prep_value(self, value, connection, prepared=False):
        if value is not None:
            value = value.ToBinary()
            return connection.Database.Binary(value)
        else:
            return value
        
    def to_python(self, value):
        if isinstance(value, ExplicitBitVect):
            return value
        else:
            if value:
                if isinstance(value, six.text_type):
                    # If it's a string, it should be base64-encoded raw data
                    value = six.memoryview(b64decode(force_bytes(value)))
                else:
                    value = bytes(value)
                return ExplicitBitVect(value)
            else:
                return ExplicitBitVect(0)

