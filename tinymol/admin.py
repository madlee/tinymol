from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from django.contrib import admin
from .models import Tag, Name, Code, Molecule, RealValue, IntValue, Keyword


class TagAdmin(admin.ModelAdmin):
    pass
admin.site.register(Tag, TagAdmin)

class NameAdmin(admin.ModelAdmin):
    pass
admin.site.register(Name, NameAdmin)

class CodeAdmin(admin.ModelAdmin):
    pass
admin.site.register(Code, CodeAdmin)

class MoleculeAdmin(admin.ModelAdmin):
    list_display = "id get_smiles inchi_key get_formula get_weight".split()
    list_display_links = "id get_smiles".split()
admin.site.register(Molecule, MoleculeAdmin)

class RealValueAdmin(admin.ModelAdmin):
    pass
admin.site.register(RealValue, RealValueAdmin)

class IntValueAdmin(admin.ModelAdmin):
    pass
admin.site.register(IntValue, IntValueAdmin)

class KeywordAdmin(admin.ModelAdmin):
    pass
admin.site.register(Keyword, KeywordAdmin)