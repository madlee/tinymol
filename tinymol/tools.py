from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function


from rdkit.Chem import Mol as Molecule, MolToSmiles as to_smiles, MolFromSmarts as parse_smarts, MolFromSmiles as parse_smiles
from rdkit.Chem import MolToMolBlock as to_mdl, MolFromMolBlock
from rdkit.DataStructs.cDataStructs import ExplicitBitVect
from rdkit.Chem.rdMolDescriptors import CalcMolFormula as formula, _CalcMolWt as weight
from rdkit.Chem.inchi import MolToInchi, InchiToInchiKey
from rdkit.Chem import RDKFingerprint
from rdkit.DataStructs import FingerprintSimilarity as similarity


def inchi_and_key(mol, options='', logLevel=None, treatWarningAsError=False):
    if mol.GetNumAtoms() == 0:
        return '', ''
    else:
        inchi = MolToInchi(mol, bytes(options), logLevel, treatWarningAsError)
        key = InchiToInchiKey(inchi)
        return inchi, key

def inchi(mol, options='', logLevel=None, treatWarningAsError=False):
    return inchi_and_key(mol, options, logLevel, treatWarningAsError)[0]
        
def inchi_key(mol, options='', logLevel=None, treatWarningAsError=False):
    return inchi_and_key(mol, options, logLevel, treatWarningAsError)[1]

def finger_print(mol):
    return RDKFingerprint(mol, fpSize=512)


def parse_mol(content):
    return MolFromMolBlock(bytes(content))