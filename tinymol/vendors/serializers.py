'''
Created on Mar 12, 2014

@author: lifej
'''

from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from rest_framework import serializers

from madlee.serializers import UserSerializer
from ..serializers import MoleculeSerializer
from .models import Vendor, Brand, Product 

class BrandSerializer(serializers.ModelSerializer):
	class Meta:
		model = Brand
		fields = ('id', 'user')

class VendorSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    brands = BrandSerializer(many=True)

    class Meta:
        model = Vendor
        fields = ('id', 'user', 'name', 'contactor', 'title', 'address', 
            'zipcode', 'fax', 'telphone', 'homepage', 'character', 'org_code', 'introduce')

   