CONTENT_FOR_POPOUT_TEXT_AREA = 
"<form class='form'>"+
"  <div class='form-group'>" +
"<textarea rows='4' cols='32' class='form-control' placeholder='Say something here.'>"+
"</textarea>"+
"</div>" +
"  <div class='form-group pull-right'>" +
  "  <button class='btn btn-sm btn-default' title='Edit in big text editor.'><i class='fa fa-edit'></i></btn></button>"+
  "  <button class='btn btn-sm btn-default' title='Cancel'><i class='fa fa-times-circle-o'></i></btn></button>"+
  "  <button class='btn  btn-success' title='Submit'><i class='fa fa-check-square-o'></i></button>"+
"</div>" +
"</form>"


var directives = angular.module('tinymolDirectives', [])

directives.directive('tinymol', function() {
  return function(scope, elements, iAttr) {
    var mol = scope.mol.structure
    mol = TME.parseMol(mol)
    var canvas = elements[0]
    var div = jQuery(canvas)
    var height = scope.$parent.panel_height*120
    var width = scope.$parent.panel_width
    width = jQuery(jQuery('.rule-'+width)[0]).width()*.9
    div.attr("height", height)
    div.attr("width", width)
    canvas.mol = mol
    TME.drawMol(canvas, canvas.mol)
  };
});



var tmcardsApp = angular.module('tmcardsApp', 
  ['ngRoute', 'xeditable', 'ui.bootstrap', 'tmcardsControllers', 'tinymolDirectives'])

tmcardsApp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
    when('/login', {templateUrl: 'login.html',   controller: 'LoginCtrl'}).
    when('/home', {templateUrl: 'board-list.html',   controller: 'BoardListCtrl'}).
    when('/new-board', {templateUrl: 'board-config.html',   controller: 'BoardConfigCtrl'}).
    when('/board/:board_id', {templateUrl: 'board-detail.html', controller: 'BoardDetailCtrl'}).
    when('/config/:board_id', {templateUrl: 'board-config.html', controller: 'BoardConfigCtrl'}).
    otherwise({redirectTo: '/home'});
}]);

Madlee.angular_config(tmcardsApp)

tmcardsApp.run(['editableOptions', function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
}]);

tmcardsApp.filter('fixed_n',function(){
    return function(v, n) {
      if (is_undefined(v) || v === null) {
        return ''
      }
      else {
        return Madlee.fixed_n(v, n)
      }
    }
});

tmcardsApp.filter('from_now',function(){
    return function(v) {
      return Madlee.from_now(v)
    }
});


var tmcardsControllers = angular.module('tmcardsControllers', []);

tmcardsControllers.controller('LoginCtrl', ['$scope', '$http', function ($scope, $http) {
  if (is_undefined(Madlee.user)) {
    jQuery('.navbar').hide()
  }
  else {
    window.location = "#/home"
  }

  $scope.login = function() {
    var username = jQuery('#txt-username').val()
    var password = jQuery('#txt-password').val()
    var remember = jQuery('#chk_remember_me').attr("checked")

    data = {username: username, password: password, keep_login: remember}

    $http.post('/madlee/user/login', data).success(function(data) {
      window.location = '#/home'
    }).error(function() {

    })
  }
}]);

tmcardsControllers.controller('BoardConfigCtrl', ['$scope', '$routeParams', '$http', function ($scope, $routeParams, $http) {
  Madlee.login_first()

  if (is_undefined($routeParams.board_id)) {
    $http.get('/madlee/user/who_am_i').success(function(data) {
      $scope.record = {
        name: 'New Board',
        note: 'Say something about this board.',
        author: data,
        timestamp0: moment().format(),
        timestamp1: moment().format()
      }  
    })
  }
  else {
    $http.get('board/'+$routeParams.board_id).success(function(data) {
      $scope.record = data
    })
  }

  $http.get('../mol/name_tags').success(function(data) {
    $scope.name_tags = data.results
  })

  jQuery('#my-tabs a').click(function (e) {
    e.preventDefault()
    jQuery(this).tab('show')
  })

  $scope.save = function() {
    if (is_undefined($scope.record.id)) {
      $http.post('board/new_board', $scope.record).success(function(data){
        window.location ="#/board/" + data.result.id
      })
    }
    else {

    }
  }
	
}]);

tmcardsControllers.controller('BoardListCtrl', ['$scope', '$routeParams', '$http', function ($scope, $routeParams, $http) {
  Madlee.login_first()
  $http.get("board").success(function(data) {
    $scope.boards = data
  })

}]);

var resize_mol_canvas = function(width, height) {
  height=height*120
  var rule = jQuery(jQuery('.rule-'+width)[0])
  width = rule.width()*.9
  jQuery('.mol-canvas').each(function(i, canvas) {
    var div = jQuery(canvas)
    div.attr("height", height)
    div.attr("width", width)

    TME.drawMol(canvas, canvas.mol)
  }) 
}

tmcardsControllers.controller('BoardDetailCtrl', ['$scope', '$routeParams', '$http', function ($scope, $routeParams, $http) {
  Madlee.login_first()

  $http.get("board/"+$routeParams.board_id).success(function(data) {
    $scope.record = data
  });

  $http.get("board/"+$routeParams.board_id + "/mol").success(function(data) {
    $scope.mols = data
  });

  $scope.set_panel_height = function(i) {
    if (i !== $scope.panel_height) {
      $scope.panel_height = i
      resize_mol_canvas($scope.panel_width, $scope.panel_height)
    }
  }
  $scope.set_panel_width = function(i) {
    if (i !== $scope.panel_width) {
      $scope.panel_width = i
      resize_mol_canvas($scope.panel_width, $scope.panel_height)
    }
  }

  $scope.set_rate = function(mol_id, i) {
    $http.post("board/"+$routeParams.board_id + "/mol/" + mol_id + "/set_rate", {rate:i}).
      success(function(data) {
        for (var i = 0; i < $scope.mols.results.length; ++i) {
          if ( $scope.mols.results[i].id === mol_id) {
            $scope.mols.results[i].rate = data.stat
          }
        }
      }).
      error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });    
  }

  $scope.comment_pops = {}

  $scope.add_comment = function($event, mol_id) {
    $scope.current_mol_id = mol_id
    $scope.current_comment = "Hello"
    jQuery('#dlg-input-comment').modal('show');
  }

  $scope.submit_comment = function($event) {
    var comment = jQuery.trim($scope.current_comment)
    if (comment) {
      var post_url = "board/"+$routeParams.board_id + "/mol/" + $scope.current_mol_id + "/add_comment"
      $http.post(post_url, {comment: $scope.current_comment}).success(function(data) {
        
        for (var i = 0; i < $scope.mols.results.length; ++i) {
          if ( $scope.mols.results[i].id === $scope.current_mol_id) {
            $scope.mols.results[i].comments.push(data.comment)
          }
        }
        jQuery('#dlg-input-comment').modal('hide');
        $scope.current_mol_id = null;
      }).error(function(error) {

      })
    }
  }

  $scope.panel_height = 1
  $scope.panel_width = 4

  
}]);


