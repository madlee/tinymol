from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from django.conf.urls import patterns, include, url
from rest_framework import routers

from .views import MoleculeViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'mol', MoleculeViewSet)

urlpatterns = patterns('tinymol.views',
	url(r'^$', 'index'),
	url(r'^vendors/', include('tinymol.vendors.urls')),
    url(r'^cards/', include('tinymol.cards.urls')),
    url(r'', include(router.urls)),
)

