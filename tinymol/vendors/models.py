from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from django.db import models
from django.conf import settings
from django.utils.encoding import python_2_unicode_compatible

from ..models import Molecule, Name

@python_2_unicode_compatible
class Vendor(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.TextField()

    contactor = models.TextField()
    title = models.CharField(max_length=1)
    address = models.TextField(blank=False)
    zipcode = models.TextField(blank=True, default='')
    fax = models.TextField(blank=True, default='')
    telphone = models.TextField(blank=True, default='')
    homepage = models.URLField(blank=True, default='')

    character = models.CharField(max_length=1, blank=True, default='')
    org_code = models.TextField(max_length=50, blank=True, default='')
    introduce = models.TextField(blank=True, default='')

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Brand(models.Model):
    name = models.ForeignKey(Name)
    vendor = models.ManyToManyField(Vendor)

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Product(models.Model):
    code = models.CharField(max_length=50, db_index=True)
    name = models.ForeignKey(Name)
    molecule = models.ForeignKey(Molecule, null=True, blank=True, default=None)
    vendor = models.ForeignKey(Vendor)
    brand = models.ForeignKey(Brand)
    note = models.TextField()

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Package(models.Model):
    product = models.ForeignKey(Product)
    specifications = models.TextField()
    purity = models.FloatField()

    def __str__(self):
        return str(self.product) + self.specifications

@python_2_unicode_compatible
class Price(models.Model):
    package = models.ForeignKey(Package)
    price = models.DecimalField(max_digits=12, decimal_places=4)
    currency = models.CharField(max_length=3, default='RMB')

    def __str__(self):
        return "%s%12.2d" % (self.currency, self.price)


@python_2_unicode_compatible
class StructureSearch(models.Model):
    latest_visit = models.DateTimeField(auto_now=True)
    critical = models.TextField()
    latest_test = models.ForeignKey(Molecule, related_name='+')
    mols = models.ManyToManyField(Molecule, related_name='+')

    def __str__(self):
        return self.critical
