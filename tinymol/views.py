'''
Created on Jul 17, 2013

@author: lifej
'''

from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function


from cStringIO import StringIO
from urllib import unquote_plus

from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route

from rdkit import Chem
from rdkit.Chem import AllChem

from madlee.views import rest_view

# from madlee.rest_views import *
from .tools import parse_mol, formula
from .serializers import Molecule, MoleculeSerializer, SmilesSerializer, Tag, TagSerializer


class MoleculeViewSet(viewsets.ModelViewSet):
    queryset = Molecule.objects.all()
    serializer_class = MoleculeSerializer

    @list_route(methods=['GET', 'POST'])
    def refine(self, request):
        moldata = unquote_plus(request.REQUEST['moldata'])
        mol = Chem.MolFromMolBlock(bytes(moldata))
        if mol == None:
            raise IOError('Illegal Molecule')
        else:
            AllChem.Compute2DCoords(mol)
            mol = Chem.MolToMolBlock(mol)
            return {'mol': mol}

    @list_route(methods=['GET', 'POST'])
    def convert(self, request):
        """Convert the input file to mol file and return it."""
        molfile = request.files.molfile
        if molfile.filename.endswith('.mol'):
            content = molfile.file.read()
            return { 'content': content }
        else:
            raise IOError('Unknown File Format')


    @list_route(methods=['GET', 'POST'])
    def highlight(self, request):
        """highlight the file ."""
        pattern = request.REQUEST['pattern']
        pattern = Chem.MolFromSmiles(bytes(pattern))

        if pattern:
            moldata = request.REQUEST['molecules']
            result = {}
            for mol in Chem.ForwardSDMolSupplier(StringIO(moldata)):
                if mol != None:
                    match = mol.GetSubstructMatch(pattern)
                    if match:
                        result[mol.GetProp('ID')] = match
            return {'matches': result}
        else:
            raise ResponseBadRequest("Invalid Molecule Pattern")

    @list_route(methods=['GET', 'POST'])
    def download(self, request):
        data = request.REQUEST['mol']
        data = unquote_plus(data)
        try:
            mol = parse_mol(data)
            filename = formula(mol)
        except:
            filename = "UNKNOWN"

        response = HttpResponse(data, content_type = 'text/plain')
        response['Content-Disposition'] = 'attachment; filename="%s.mol"' % filename
        return response

    @list_route(methods=['GET'])
    @rest_view
    def name_tags(self, request):
        queryset = Tag.objects.filter(names__isnull=False).distinct()
        results = TagSerializer(queryset, many=True).data
        return {'count': len(results), 'results': results}

    def get_queryset(self):
        query = Molecule.objects.all()
        text = self.request.QUERY_PARAMS.get('text', '').strip()

        if text:
            query1 = query.filter(codes__value__istartswith=text)
        
            if query1.count() == 0:
                query = query.filter(names__value__icontains=text).distinct()
            else:
                query = query1
        
        return query
        
def index(request):
    return HttpResponseRedirect('/tinymol/index.html')
