from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function

from hashlib import md5
from base64 import b64encode
from threading import Thread
from time import sleep
from datetime import datetime as DateTime

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from .fields import MoleculeField, BitsetField
from .tools import formula, weight, inchi_key, finger_print, to_smiles, similarity, parse_smarts, to_mdl, ExplicitBitVect



@python_2_unicode_compatible
class Tag(models.Model):
    label = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.label

@python_2_unicode_compatible
class Unit(models.Model):
    label = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.label

@python_2_unicode_compatible
class Name(models.Model):
    type = models.ForeignKey(Tag, related_name='names')
    value = models.TextField(blank=False)
    language = models.ForeignKey(Tag, related_name='languages')

    def __str__(self):
        return self.value

    class Meta:
        unique_together = ('type', 'value'),

@python_2_unicode_compatible
class Code(models.Model):
    type = models.ForeignKey(Tag, db_index=True, related_name='+')
    value = models.TextField(blank=False)

    def __str__(self):
        return self.value

    class Meta:
        unique_together = ('type', 'value'),

@python_2_unicode_compatible
class Molecule(models.Model):
    structure = MoleculeField(null=False, blank=False)
    inchi_key = models.CharField(max_length=28, db_index=True)
    finger_print = BitsetField(null=False, blank=False) 
    
    codes = models.ManyToManyField(Code, db_index=True)
    names = models.ManyToManyField(Name, db_index=True)
    
    def get_formula(self):
        return formula(self.structure)
    
    def get_weight(self):
        return weight(self.structure)

    def get_smiles(self):
        return to_smiles(self.structure)

    def get_mdl(self):
        return to_mdl(self.structure)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.inchi_key = inchi_key(self.structure)
        self.finger_print = finger_print(self.structure)
        
        super(Molecule, self).save(force_insert, force_update, using, update_fields)
        
    def __str__(self):
        return "[%s]" % self.get_smiles()

@python_2_unicode_compatible
class RealValue(models.Model):
    type = models.ForeignKey(Tag, related_name='+')
    mol = models.ForeignKey(Molecule, related_name='real_props')
    qualify = models.CharField(max_length=1, default='')
    value = models.FloatField()
    unit = models.ForeignKey(Unit, null=True, default=None)
    remark = models.TextField(default='')

    def __str__(self):
        return "%s: %s" % (self.type.label, self.value)

    class Meta:
        index_together = "type value".split(),

@python_2_unicode_compatible
class IntValue(models.Model):
    type = models.ForeignKey(Tag, related_name='+')
    mol = models.ForeignKey(Molecule, related_name='int_props')
    value = models.IntegerField()
    remark = models.TextField(default='')

    def __str__(self):
        return "%s: %s" % (self.type.label, self.value)

    class Meta:
        index_together = "type value".split(),


@python_2_unicode_compatible
class Keyword(models.Model):
    word = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.word


