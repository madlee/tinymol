from __future__ import unicode_literals
from __future__ import division
from __future__ import print_function


'''
Created on Apr 12, 2013

@author: lifej
'''

from django.conf.urls import patterns, url, include
from rest_framework import routers
from .views import VendorViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'vendor', VendorViewSet)

urlpatterns = patterns('tinymol.vendors.views',
	url(r'structure_search', 'structure_search'),
	url(r'', include(router.urls)),
)

